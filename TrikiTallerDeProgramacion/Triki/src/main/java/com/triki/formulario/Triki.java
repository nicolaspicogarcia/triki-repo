package com.triki.formulario;

import com.triki.algoritmo.Algoritmo;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import org.apache.log4j.Logger;

public class Triki extends javax.swing.JFrame {

    static final Logger logger = Logger.getLogger(Triki.class);
    private Algoritmo algoritmo;
    private final String figuras[] = {"O", "0", "X"};

    public Triki() {
        initComponents();
        inicializarComponentes();
        ocultarFiguras();
        centrarFiguras();
    }

    private void inicializarComponentes() {
        restart.setVisible(false);
        algoritmo = new Algoritmo();
        //cambiar color
        jp_contenedor.setBackground(Color.white);
        //Cambiar color
        jLabel2.setForeground(Color.RED);
        this.setSize(550, 500);
        this.setLocationRelativeTo(null);
    }

    private void centrarFiguras() {
        jLabel2.setHorizontalAlignment(JLabel.CENTER);
        figura.setHorizontalAlignment(JLabel.CENTER);
        figura2.setHorizontalAlignment(JLabel.CENTER);
        figura1.setHorizontalAlignment(JLabel.CENTER);
        figura3.setHorizontalAlignment(JLabel.CENTER);
        figura4.setHorizontalAlignment(JLabel.CENTER);
        figura5.setHorizontalAlignment(JLabel.CENTER);
        figura8.setHorizontalAlignment(JLabel.CENTER);
        figura7.setHorizontalAlignment(JLabel.CENTER);
        figura6.setHorizontalAlignment(JLabel.CENTER);
    }

    private void ocultarFiguras() {
        figura.setVisible(false);
        figura2.setVisible(false);
        figura1.setVisible(false);
        figura3.setVisible(false);
        figura4.setVisible(false);
        figura5.setVisible(false);
        figura8.setVisible(false);
        figura7.setVisible(false);
        figura6.setVisible(false);
    }

    private void mostrarFiguras() {
        figura.setVisible(true);
        figura2.setVisible(true);
        figura1.setVisible(true);
        figura3.setVisible(true);
        figura4.setVisible(true);
        figura5.setVisible(true);
        figura8.setVisible(true);
        figura7.setVisible(true);
        figura6.setVisible(true);
    }

    private void quitarFiguras() {
        figura.setIcon(null);
        figura2.setIcon(null);
        figura1.setIcon(null);
        figura3.setIcon(null);
        figura4.setIcon(null);
        figura5.setIcon(null);
        figura8.setIcon(null);
        figura7.setIcon(null);
        figura6.setIcon(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jp_contenedor = new javax.swing.JPanel();
        figura8 = new javax.swing.JLabel();
        figura7 = new javax.swing.JLabel();
        figura6 = new javax.swing.JLabel();
        figura5 = new javax.swing.JLabel();
        figura4 = new javax.swing.JLabel();
        figura3 = new javax.swing.JLabel();
        figura1 = new javax.swing.JLabel();
        figura2 = new javax.swing.JLabel();
        figura = new javax.swing.JLabel();
        tablero = new javax.swing.JLabel();
        play = new javax.swing.JButton();
        restart = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jp_contenedor.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        figura8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/x.png"))); // NOI18N
        figura8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                figura8MouseClicked(evt);
            }
        });
        jp_contenedor.add(figura8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 240, 160, 80));

        figura7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/x.png"))); // NOI18N
        figura7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                figura7MouseClicked(evt);
            }
        });
        jp_contenedor.add(figura7, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 240, 160, 80));

        figura6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/x.png"))); // NOI18N
        figura6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                figura6MouseClicked(evt);
            }
        });
        jp_contenedor.add(figura6, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 240, 160, 80));

        figura5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/x.png"))); // NOI18N
        figura5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                figura5MouseClicked(evt);
            }
        });
        jp_contenedor.add(figura5, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 160, 160, 80));

        figura4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/x.png"))); // NOI18N
        figura4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                figura4MouseClicked(evt);
            }
        });
        jp_contenedor.add(figura4, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 160, 160, 80));

        figura3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/x.png"))); // NOI18N
        figura3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                figura3MouseClicked(evt);
            }
        });
        jp_contenedor.add(figura3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 160, 160, 80));

        figura1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/x.png"))); // NOI18N
        figura1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                figura1MouseClicked(evt);
            }
        });
        jp_contenedor.add(figura1, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 70, 160, 80));

        figura2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/x.png"))); // NOI18N
        figura2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                figura2MouseClicked(evt);
            }
        });
        jp_contenedor.add(figura2, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 70, 150, 80));

        figura.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/x.png"))); // NOI18N
        figura.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
                figuraAncestorAdded(evt);
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        figura.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                figuraMouseClicked(evt);
            }
        });
        jp_contenedor.add(figura, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 160, 80));

        tablero.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Fondo.PNG"))); // NOI18N
        jp_contenedor.add(tablero, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, -1, -1));

        play.setText("Jugar");
        play.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                playActionPerformed(evt);
            }
        });
        jp_contenedor.add(play, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 390, -1, -1));

        restart.setText("Reinicar");
        restart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                restartActionPerformed(evt);
            }
        });
        jp_contenedor.add(restart, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 390, -1, -1));

        jLabel1.setText("TRIKI VERSION 1.0");
        jp_contenedor.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 20, -1, -1));

        jLabel2.setText("Dar clic en el boton jugar ");
        jp_contenedor.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 350, 490, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jp_contenedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jp_contenedor, javax.swing.GroupLayout.PREFERRED_SIZE, 513, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void playActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_playActionPerformed
        play.setVisible(false);
        restart.setVisible(true);
        jLabel2.setText("");
        jLabel2.setVisible(false);
        quitarFiguras();
        mostrarFiguras();
        algoritmo.empezarPartida();
    }//GEN-LAST:event_playActionPerformed

    private void figuraAncestorAdded(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_figuraAncestorAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_figuraAncestorAdded

    private void figuraMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_figuraMouseClicked
        try {
            if (ckeckStatus()) {
                colocarImagen(figura, "circle.png");
                figura.setVisible(true);
                figura.repaint();
                algoritmo.verificarPosicion(0, 0);
                pintarFigura();
                checkWinner();
            } else {
                jLabel2.setVisible(true);
                jLabel2.setText("Reinice la partida...");
            }
        } catch (Exception e) {
            logger.error("Se presentaron problemas con la posicion 0,0 ", e);
        }
    }//GEN-LAST:event_figuraMouseClicked

    private void figura2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_figura2MouseClicked
        try {
            if (ckeckStatus()) {
                colocarImagen(figura2, "circle.png");
                figura2.setVisible(true);
                figura2.repaint();
                algoritmo.verificarPosicion(0, 1);
                pintarFigura();
                checkWinner();
            } else {
                jLabel2.setVisible(true);
                jLabel2.setText("Reinice la partida...");
            }
        } catch (Exception e) {
            logger.error("Se presentaron problemas con la posicion 0,1 ", e);
        }
    }//GEN-LAST:event_figura2MouseClicked

    private void figura1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_figura1MouseClicked
        try {
            if (ckeckStatus()) {
                colocarImagen(figura1, "circle.png");
                figura1.setVisible(true);
                figura1.repaint();
                algoritmo.verificarPosicion(0, 2);
                pintarFigura();
                checkWinner();
            } else {
                jLabel2.setVisible(true);
                jLabel2.setText("Reinice la partida...");
            }
        } catch (Exception e) {
            logger.error("Se presentaron problemas con la posicion 0,2 ", e);
        }
    }//GEN-LAST:event_figura1MouseClicked

    private void figura3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_figura3MouseClicked
        try {
            if (ckeckStatus()) {
                colocarImagen(figura3, "circle.png");
                figura3.setVisible(true);
                figura3.repaint();
                algoritmo.verificarPosicion(1, 0);
                pintarFigura();
                checkWinner();
            } else {
                jLabel2.setVisible(true);
                jLabel2.setText("Reinice la partida...");
            }
        } catch (Exception e) {
            logger.error("Se presentaron problemas con la posicion 1,0 ", e);
        }
    }//GEN-LAST:event_figura3MouseClicked

    private void figura4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_figura4MouseClicked
        try {
            if (ckeckStatus()) {
                colocarImagen(figura4, "circle.png");
                figura4.setVisible(true);
                figura4.repaint();
                algoritmo.verificarPosicion(1, 1);
                pintarFigura();
                checkWinner();
            } else {
                jLabel2.setVisible(true);
                jLabel2.setText("Reinice la partida...");
            }
        } catch (Exception e) {
            logger.error("Se presentaron problemas con la posicion 1,1 ", e);
        }
    }//GEN-LAST:event_figura4MouseClicked

    private void figura5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_figura5MouseClicked
        try {
            if (ckeckStatus()) {
                colocarImagen(figura5, "circle.png");
                figura5.setVisible(true);
                figura5.repaint();
                algoritmo.verificarPosicion(1, 2);
                pintarFigura();
                checkWinner();
            } else {
                jLabel2.setVisible(true);
                jLabel2.setText("Reinice la partida...");
            }
        } catch (Exception e) {
            logger.error("Se presentaron problemas con la posicion 1,2 ", e);
        }
    }//GEN-LAST:event_figura5MouseClicked

    private void figura8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_figura8MouseClicked
        try {
            if (ckeckStatus()) {
                colocarImagen(figura8, "circle.png");
                figura8.setVisible(true);
                figura8.repaint();
                algoritmo.verificarPosicion(2, 0);
                pintarFigura();
                checkWinner();
            } else {
                jLabel2.setVisible(true);
                jLabel2.setText("Reinice la partida...");
            }
        } catch (Exception e) {
            logger.error("Se presentaron problemas con la posicion 2,0 ", e);
        }
    }//GEN-LAST:event_figura8MouseClicked

    private void figura7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_figura7MouseClicked
        try {
            logger.info(" ckeckStatus " + ckeckStatus());
            if (ckeckStatus()) {
                colocarImagen(figura7, "circle.png");
                figura7.setVisible(true);
                figura7.repaint();
                algoritmo.verificarPosicion(2, 1);
                pintarFigura();
                checkWinner();
            } else {
                jLabel2.setVisible(true);
                jLabel2.setText("Reinice la partida...");
            }
        } catch (Exception e) {
            logger.error("Se presentaron problemas con la posicion 2,1 ", e);
        }
    }//GEN-LAST:event_figura7MouseClicked

    private boolean ckeckStatus() {
        return !jLabel2.isVisible() && (figura.getIcon() == null || figura2.getIcon() == null
                || figura1.getIcon() == null || figura3.getIcon() == null || figura4.getIcon() == null
                || figura5.getIcon() == null || figura8.getIcon() == null || figura7.getIcon() == null
                || figura6.getIcon() == null);
    }

    private void checkWinner() {
        int estado = algoritmo.obtenerGanador();
        if (estado == 0) {
            jLabel2.setVisible(true);
            jLabel2.setText("Ganaste");
        } else if (estado == 1) {
            jLabel2.setVisible(true);
            jLabel2.setText("Perdiste");
        }
    }

    private void figura6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_figura6MouseClicked
        try {
            if (ckeckStatus()) {
                colocarImagen(figura6, "circle.png");
                figura6.setVisible(true);
                figura6.repaint();
                algoritmo.verificarPosicion(2, 2);
                pintarFigura();
                checkWinner();
            } else {
                jLabel2.setVisible(true);
                jLabel2.setText("Reinice la partida...");
            }
        } catch (Exception e) {
            logger.error("Se presentaron problemas con la posicion 2,2 ", e);
        }
    }//GEN-LAST:event_figura6MouseClicked

    private void restartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_restartActionPerformed
        jLabel2.setText("");
        jLabel2.setVisible(false);
        quitarFiguras();
        mostrarFiguras();
        algoritmo.empezarPartida();
    }//GEN-LAST:event_restartActionPerformed

    private void colocarImagen(JLabel label, String nombreImagen) {
        ImageIcon icon = new ImageIcon(getClass().getResource("/image/" + nombreImagen));
        label.setIcon(icon);
    }

    private void pintarFigura() {
        int tmp[][] = algoritmo.devolverTablero();
        for (int n = 0; n < 3; n++) {
            for (int m = 0; m < 3; m++) {
                if (figuras[tmp[n][m] + 1].equals("X")) {
                    JLabel label = obtenerLabel(n, m);
                    colocarImagen(label, "x.png");
                    label.setVisible(true);
                    label.repaint();
                }
            }
        }
    }

    private JLabel obtenerLabel(int x, int y) {
        JLabel label = new JLabel();
        String posicion = String.valueOf(x + "" + y);
        switch (posicion) {
            case "00":
                label = figura;
                break;
            case "01":
                label = figura2;
                break;
            case "02":
                label = figura1;
                break;
            case "10":
                label = figura3;
                break;
            case "11":
                label = figura4;
                break;
            case "12":
                label = figura5;
                break;
            case "20":
                label = figura8;
                break;
            case "21":
                label = figura7;
                break;
            case "22":
                label = figura6;
                break;
        }
        return label;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Triki.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Triki.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Triki.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Triki.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Triki().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel figura;
    private javax.swing.JLabel figura1;
    private javax.swing.JLabel figura2;
    private javax.swing.JLabel figura3;
    private javax.swing.JLabel figura4;
    private javax.swing.JLabel figura5;
    private javax.swing.JLabel figura6;
    private javax.swing.JLabel figura7;
    private javax.swing.JLabel figura8;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jp_contenedor;
    private javax.swing.JButton play;
    private javax.swing.JButton restart;
    private javax.swing.JLabel tablero;
    // End of variables declaration//GEN-END:variables
}
